package elevenslab;


/**
 * This is a class that tests the Deck class.
 */
public class DeckTester {

	/**
	 * The main method in this class checks the Deck operations for consistency.
	 *	@param args is not used.
	 */
	public static void main(String[] args) {
                String[] rank = {"jack", "queen", "king"};
		String[] suit = {"blue", "red"};
		int[] pointValue = {11, 12, 13};
		Deck d = new Deck(rank, suit, pointValue);
                System.out.println(d);
                String[] rarity = {"Rare", "Epic", "Legendary"};
		String[] tribe = {"Goblins", "Gnomes", "Classic"};
		int[] attackValue = {4, 6, 9};
		Deck hearth = new Deck(rarity, tribe, attackValue);
                System.out.println(hearth);
                String[] fish = {"polar bear", "giraffe", "lion"};
		String[] clan = {"artic", "africa"};
		int[] childValue = {1, 2, 18};
		Deck gofsh = new Deck(fish, clan, childValue);
                System.out.println(gofsh);
                
                
                String[] rank2 = { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eighte", "Nine", "Ten", "King", "Queen", "Jack"};
		String[] suit2 = {"Spades", "Hearts", "Diamonds", "Clubs"};
		int[] pointValueTwo = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
		Deck twantyOne = new Deck(rank2, suit2, pointValueTwo);
                System.out.println(twantyOne);
                
                

		//System.out.println("**** Original Deck Methods ****");
		//System.out.println("  toString:\n" + d.toString());
		//System.out.println("  isEmpty: " + d.isEmpty());
		//System.out.println("  size: " + d.size());
		//System.out.println();
		//System.out.println();

		//System.out.println("**** Deal a Card ****");
		//System.out.println("  deal: " + d.deal());
		//System.out.println();
		//System.out.println();

		//System.out.println("**** Deck Methods After 1 Card Dealt ****");
		//System.out.println("  toString:\n" + d.toString());
		//System.out.println("  isEmpty: " + d.isEmpty());
		//System.out.println("  size: " + d.size());
		//System.out.println();
		//System.out.println();

		//System.out.println("**** Deal Remaining 5 Cards ****");
		//for (int i = 0; i < 5; i++) {
		//	System.out.println("  deal: " + d.deal());
		//}
		//System.out.println();
		///System.out.println();

		//System.out.println("**** Deck Methods After All Cards Dealt ****");
		//System.out.println("  toString:\n" + d.toString());
		//System.out.println("  isEmpty: " + d.isEmpty());
		//System.out.println("  size: " + d.size());
		//System.out.println();
		//System.out.println();

		//System.out.println("**** Deal a Card From Empty Deck ****");
		//System.out.println("  deal: " + d.deal());
		//System.out.println();
		//System.out.println();

		/* *** TO BE COMPLETED IN ACTIVITY 4 *** */
	}
}
